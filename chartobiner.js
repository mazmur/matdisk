// Latihan 3
// # npm install readline-sync

var readline = require('readline-sync');

var n = readline.question("Masukkan karakter : ");

var dec = n.charCodeAt();
var biner = parseInt(dec, 10).toString(2);
var hexa = parseInt(dec, 10).toString(16);
var octa = parseInt(dec, 10).toString(8);

console.log("Karakter = " + n + ", Decimal = " + dec + ", Binner = " + biner + ", Hexa = " + hexa + ", Octa = " + octa);