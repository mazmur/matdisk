// Program Menentukan konversi hari dari angka ke string dengan switch


var readline = require('readline-sync');

var Nilai = parseInt(readline.question("Masukkan Hari (Angka) : "));

switch (Nilai) {
    case 1:
        console.log("Hari SENIN");
        break;
    case 2:
        console.log("Hari SELASA");
        break;
    case 3:
        console.log("Hari RABU");
        break;
    case 4:
        console.log("Hari KAMIS");
        break;
    case 5:
        console.log("Hari JUMAT");
        break;
    case 6:
        console.log("Hari SABTU");
        break;
    default:
        console.log("Hari MINGGU");
}