//Array 1 Dimensi
var students = ["herman", "joy", "santi", "chaleb", 5, 10];

//Array 2 Dimensi
var A = [
    [2, 4],
    [3, 5]
];
var B = [
    [3, 4],
    [7, 6]
];

var hasil = [
    [A[0][0] + B[0][0], A[0][1] + B[0][1]],
    [A[1][0] + B[1][0], A[1][1] + B[1][1]]
];

console.log(hasil);