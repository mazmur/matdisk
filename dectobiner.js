// Latihan 3
// # npm install readline-sync

var readline = require('readline-sync');

var n = readline.question("Masukkan nilai desimal : ");

var biner = parseInt(n, 10).toString(2);
var hexa = parseInt(n, 10).toString(16);
var octa = parseInt(n, 10).toString(8);

console.log("Decimal = " + n + ", Binner = " + biner + ", Hexa = " + hexa + ", Octa = " + octa);