// Program Trapesium dan kondisional


var readline = require('readline-sync');

var a = parseInt(readline.question("Masukkan panjang sisi a : "));
var b = parseInt(readline.question("Masukkan panjang sisi b : "));
var t = parseInt(readline.question("Masukkan tinggi t : "));

if ((a > b) & (t > 0)) {
    var L = ((a + b) * t) / 2;
    console.log("Luas Trapesium =  " + L);
} else {
    console.log("Sisi a tidak boleh lebih pendek dari sisi b");
}