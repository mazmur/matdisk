// Program Menentukan Nilai dengan kondisional


var readline = require('readline-sync');

var Nilai = parseInt(readline.question("Masukkan Nilai Mahasiswa (Angka) : "));


if (Nilai > 80) {
    console.log("Nilai Mahasiswa (Huruf) : A");
} else if (69 < Nilai & Nilai < 81) {
    console.log("Nilai Mahasiswa (Huruf) : B");
} else if (59 < Nilai & Nilai < 70) {
    console.log("Nilai Mahasiswa (Huruf) : C");
} else if (49 < Nilai & Nilai < 60) {
    console.log("Nilai Mahasiswa (Huruf) : D");
} else {
    console.log("Nilai Mahasiswa (Huruf) : E");
}