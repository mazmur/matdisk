//test1
var x = 1;

var students = ["herman", "joy", "santi", "chaleb", 5, 10];

var dosen = ["Agung", "Maimun", "Imam", "Bu Ade", "Kundang"]

var buku = ["Kopingho", "Dilan", "Pancasila", "IPA", "IPS"]

console.log(students);
console.log(x);
console.log(dosen);
console.log(buku);

console.log(students[2]);
console.log("Elemen4 + element5 = " + (students[4] + students[5]));

// //menambahkan isi element array di depan
// students.unshift("ita");
// console.log(students);

// //menambahkan isi element array di belakang
// students.push("tarigan");
// console.log(students);

// //menyisipkan isi element ke 3
// students.splice(3, 0, 'doni');
// console.log(students);

// //mengganti  isi element ke 3
// students.splice(3, 1, 'joy');
// console.log(students);