var readline = require('readline-sync');

var n = parseInt(readline.question("Masukkan jumlah baris  : "));

var hasil = 0;

for (var i = 1; i <= n; i++) {
    hasil = i % 2; //modulus --> sisa hasil bagi
    if (hasil == 1) {
        console.log(i + ' Hello');
    } else {
        console.log(i + ' Programming');
    }
}