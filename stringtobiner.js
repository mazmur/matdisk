// Latihan 3
// # npm install readline-sync

var readline = require('readline-sync');

// penampung nilai berupa array
var Desimal, Binner, Heksa, Okta, i, nilai, tampung;
Desimal = [];
Binner = [];
Heksa = [];
Okta = [];


var kalimat = readline.question("Masukkan Kalimat : ");
var panjang = kalimat.length;

for (i = 0; i < panjang; i++) {
    //ambil desimal
    nilai = kalimat.charCodeAt(i);
    Desimal.push(nilai);

    //ambil binner
    tampung = parseInt(nilai, 10).toString(2);
    Binner.push(tampung);

    //ambil heksa
    tampung = parseInt(nilai, 10).toString(16);
    Heksa.push(tampung);

    //ambil okta
    tampung = parseInt(nilai, 10).toString(8);
    Okta.push(tampung);
}

console.log("Desimal = " + Desimal);
console.log("Binner = " + Binner);
console.log("Heksa = " + Heksa);
console.log("Octa = " + Okta);